import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  apiBaseURL =  environment.ApiBaseURL;
  position_Api = this.apiBaseURL + 'Position';
  separation_Api = this.apiBaseURL + 'Separation';
  status_API = this.apiBaseURL + 'ProductionStatus';
  stage_API = this.apiBaseURL + 'ProductionStage';

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;

  constructor(private http: HttpClient) {
      this.headers = new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
      });
      this.sfheaders = new HttpHeaders({
          RequestVerificationToken: 'token'
      });
  }


  getAllPosition(): Observable<any> {
      return this.http.get(this.position_Api);
  }

  savePosition(PositionName: any): Observable<any> {
      const body = new HttpParams().set('Name', PositionName).toString();
      return this.http.post(this.position_Api, body, { headers: this.headers });
  }

  updatePosition(PMPosition: any): Observable<any> {
      const body = new HttpParams()
          .set('Name', PMPosition.PositionName)
          .set('PositionId', PMPosition.PositionId);
      const requestBody = body.toString();
      return this.http.put(this.position_Api, requestBody, { headers: this.headers });
  }

  deletePosition(PositionId): Observable<any> {
      return this.http.delete(this.position_Api + '/' + PositionId, { headers: this.headers });
  }


  getAllSeparation(): Observable<any> {
      return this.http.get(this.separation_Api);
  }

  saveSeparation(SeparationName: any): Observable<any> {
      const body = new HttpParams().set('Name', SeparationName).toString();
      return this.http.post(this.separation_Api, body, { headers: this.headers });
  }

  updateSeparation(PMSeparation: any): Observable<any> {
      const body = new HttpParams()
          .set('Name', PMSeparation.SeparationName)
          .set('SeparationId', PMSeparation.SeparationId);
      const requestBody = body.toString();
      return this.http.put(this.separation_Api, requestBody, { headers: this.headers });
  }

  deleteSeparation(SeparationId): Observable<any> {
      return this.http.delete(this.separation_Api + '/' + SeparationId, { headers: this.headers });
  }


  getAllStatus(): Observable<any> {
      return this.http.get(this.status_API);
  }

  saveStatus(ProductionStatusName: any): Observable<any> {
      const body = new HttpParams().set('Name', ProductionStatusName).toString();
      return this.http.post(this.status_API, body, { headers: this.headers });
  }

  updateStatus(PMStatus: any): Observable<any> {
      const body = new HttpParams()
          .set('Name', PMStatus.StatusName)
          .set('ProductionStatusId', PMStatus.ProductionStatusId);
      const requestBody = body.toString();
      return this.http.put(this.status_API, requestBody, { headers: this.headers });
  }

  deleteStatus(ProductionStatusId): Observable<any> {
      return this.http.delete(this.status_API + '/' + ProductionStatusId, { headers: this.headers });
  }


  getAllStages(): Observable<any> {
      return this.http.get(this.stage_API);
  }

  saveStages(ProductionStageName: any): Observable<any> {
      const body = new HttpParams().set('Name', ProductionStageName).toString();
      return this.http.post(this.stage_API, body, { headers: this.headers });
  }

  updateStages(PMStage: any): Observable<any> {
      const body = new HttpParams()
          .set('Name', PMStage.StageName)
          .set('ProductionStageId', PMStage.ProductionStageId).toString();
      return this.http.put(this.stage_API, body, { headers: this.headers });
  }

  deleteStages(ProductionStageId): Observable<any> {
      return this.http.delete(this.stage_API + '/' + ProductionStageId, { headers: this.headers });
  }

}

