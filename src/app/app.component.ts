import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { Validators } from '@angular/forms';
import { AppService } from '../app/app.service';
import Swal from 'sweetalert2';
import { environment } from '../environments/environment';
import { fail } from 'assert';
declare var $: any;

@Component({
  selector: 'asi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  productionManagementPositionForm: FormGroup;
  productionManagementSeparationForm: FormGroup;
  productionManagementStatusForm: FormGroup;
  productionManagementStageForm: FormGroup;

  isProductionManagementPositionFormSubmitted = false;
  isProductionManagementSeparationFormSubmitted = false;
  isProductionManagementStatusFormSubmitted = false;
  isProductionManagementStageFormSubmitted = false;

  pmPositions = [];
  pmSeparations = [];
  pmStatuses = [];
  pmStages = [];

  pmPositionsModel: any = {};
  pmSeparationsModel: any = {};
  pmStatusesModel: any = {};
  pmStagesModel: any = {};

  pmPosition_PositionId = 0;
  pmSeparation_SeparationId = 0;
  pmStatus_ProductionStatusId = 0;
  pmStage_ProductionStageId = 0;

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';

  errorMessage = 'Something went wrong !';
  isLoading = true;

  constructor(private appService: AppService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService) { ToastrModule.forRoot(); }

  ngOnInit() {
    let url = window.location.href;
    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
    }
    location.replace(url + '#/productionManagement');
    this.getToken();
    this.validation();
    this.getAllPosition();
    this.getAllSeparation();
    this.getAllStatus();
    this.getAllStages();
  }
  get isValidPositionName(){
    return this.productionManagementPositionForm.get('PositionName').invalid  && (this.isProductionManagementPositionFormSubmitted
      ||  this.productionManagementPositionForm.get('PositionName').dirty ||  this.productionManagementPositionForm.get('PositionName').touched);
  }
  get isValidSeparationName(){
    return this.productionManagementSeparationForm.get('SeparationName').invalid  && (this.isProductionManagementSeparationFormSubmitted
      ||  this.productionManagementSeparationForm.get('SeparationName').dirty ||  this.productionManagementSeparationForm.get('SeparationName').touched);
  }
 
  get isValidProductionStatusName(){
    return this.productionManagementStatusForm.get('ProductionStatusName').invalid  && (this.isProductionManagementStatusFormSubmitted
      ||  this.productionManagementStatusForm.get('ProductionStatusName').dirty ||  this.productionManagementStatusForm.get('ProductionStatusName').touched);
  }
  get isValidProductionStageName(){
    return this.productionManagementStageForm.get('ProductionStageName').invalid  && (this.isProductionManagementStageFormSubmitted
      ||  this.productionManagementStageForm.get('ProductionStageName').dirty ||  this.productionManagementStageForm.get('ProductionStageName').touched);
  }
  validation() {
    this.productionManagementPositionFormValidation();
    this.productionManagementSeparationFormValidation();
    this.productionManagementStatusFormvalidation();
    this.productionManagementStageFormValidation();
  }

  productionManagementPositionFormValidation() {
    this.productionManagementPositionForm = this.formBuilder.group({
      'PositionName': [null, Validators.required]
    });
  }

  productionManagementSeparationFormValidation() {
    this.productionManagementSeparationForm = this.formBuilder.group({
      'SeparationName': [null, Validators.required]
    });
  }

  productionManagementStatusFormvalidation() {
    this.productionManagementStatusForm = this.formBuilder.group({
      'ProductionStatusName': [null, Validators.required]
    });
  }

  productionManagementStageFormValidation() {
    this.productionManagementStageForm = this.formBuilder.group({
      'ProductionStageName': [null, Validators.required]
    });
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }
  // ------------- 1) Position --------------
  getAllPosition() {
    try {
      this.appService.getAllPosition().subscribe(result => {
        if (result.StatusCode === 1) {
          this.isLoading = false;
          this.pmPositions = result.Data;
        } else {
          this.isLoading = false;
          this.pmPositions = [];
        }
      }, error => {
        this.isLoading = false;
        this.toastr.error(this.errorMessage, '');
      });
    } catch (error) {
      console.log(error);
    }


  }

  savePosition() {
    try {
      this.isProductionManagementPositionFormSubmitted = true;
      if (this.productionManagementPositionForm.valid) {
        if (this.pmPosition_PositionId === 0) {
          const positionName = this.productionManagementPositionForm.controls['PositionName'].value;
          this.appService.savePosition(positionName).subscribe(result => {
            if (result.StatusCode === 1) {
              this.resetPositionVariables();
              this.getAllPosition();
              this.toastr.success('Save successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else {
          this.pmPositionsModel.PositionName = this.productionManagementPositionForm.controls['PositionName'].value;
          this.pmPositionsModel.PositionId = this.pmPosition_PositionId;
          this.appService.updatePosition(this.pmPositionsModel).subscribe(result => {
            if (result.StatusCode === 1) {
              this.clearPosition();
              this.getAllPosition();
              this.toastr.success('Save successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        }
      } else {
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }

  editPosition(PMPosition: any) {
    try {
      this.pmPosition_PositionId = PMPosition.PositionId;
      this.productionManagementPositionForm.controls['PositionName'].setValue(PMPosition.Name);
      document.querySelector('#btnPosition').innerHTML = 'Update';
      document.getElementById('btnCancelPosition').style.display = 'inline-block';
    } catch (error) {
      this.isLoading = false;
      this.toastr.error(this.errorMessage, '');
    }
  }

  deletePosition(objPMPosition: any) {
    try {

      Swal({
        title: 'Are you sure you want to delete this production ?',
        text: '',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true,
        focusConfirm: false
      }).then((res) => {
        if (res.value) {
          this.appService.deletePosition(objPMPosition.PositionId).subscribe(result => {
            if (result.StatusCode === 1) {
              document.querySelector('#btnPosition').innerHTML = 'Add';
              this.resetPositionVariables();
              this.getAllPosition();
              this.toastr.success('Deleted successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else if (res.dismiss === Swal.DismissReason.cancel) {
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  resetPositionVariables() {
    this.productionManagementPositionForm.reset();
    this.pmPosition_PositionId = 0;
    this.isProductionManagementPositionFormSubmitted = false;
    this.pmPositionsModel = {};
  }

  clearPosition() {
    document.querySelector('#btnPosition').innerHTML = 'Add';
    document.getElementById('btnCancelPosition').style.display = 'none';
    this.resetPositionVariables();
  }

  //step-2) Seperation 
  getAllSeparation() {
    try {
      this.appService.getAllSeparation().subscribe(result => {
        if (result.StatusCode === 1) {
          this.pmSeparations = result.Data;
        } else {
          this.pmSeparations = [];
        }
      }, error => {
        this.isLoading = false;
        this.toastr.error(this.errorMessage, '');
      });
    } catch (error) {
      console.log(error);
    }


  }

  saveSeparation() {
    try {
      this.isProductionManagementSeparationFormSubmitted = true;
      if (this.productionManagementSeparationForm.valid) {
        if (this.pmSeparation_SeparationId === 0) {
          const separationName = this.productionManagementSeparationForm.controls['SeparationName'].value;
          this.appService.saveSeparation(separationName).subscribe(result => {
            if (result.StatusCode === 1) {
              this.resetSeparationVariables();
              this.getAllSeparation();
              this.toastr.success('Save successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else {
          this.pmSeparationsModel.SeparationName = this.productionManagementSeparationForm.controls['SeparationName'].value;
          this.pmSeparationsModel.SeparationId = this.pmSeparation_SeparationId;
          this.appService.updateSeparation(this.pmSeparationsModel).subscribe(result => {
            if (result.StatusCode === 1) {
              this.clearSeparation();
              this.getAllSeparation();
              this.toastr.success('Save successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        }
      } else {
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }

  editSeparation(PMSeparation: any) {
    try {
      document.querySelector('#btnSeparation').innerHTML = 'Update';
      document.getElementById('btnCancelSeparation').style.display = 'inline-block';
      this.pmSeparation_SeparationId = PMSeparation.SeparationId;
      this.productionManagementSeparationForm.controls['SeparationName'].setValue(PMSeparation.Name);
    } catch (error) {
      this.isLoading = false;
      this.toastr.error(this.errorMessage, '');
    }
  }

  deleteSeparation(objPMSeparation: any) {
    try {
      Swal({
        title: 'Are you sure you want to delete this production ?',
        text: '',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true,
        focusConfirm: false
      }).then((res) => {
        if (res.value) {
          this.appService.deleteSeparation(objPMSeparation.SeparationId).subscribe(result => {
            if (result.StatusCode === 1) {
              document.querySelector('#btnSeparation').innerHTML = 'Add';
              this.productionManagementSeparationForm.reset();
              this.pmSeparation_SeparationId = 0;
              this.getAllSeparation();
              this.toastr.success('Deleted successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else if (res.dismiss === Swal.DismissReason.cancel) {
        }
      });
    } catch (error) {
      this.isLoading = false;
      this.toastr.error(this.errorMessage, '');
    }
  }

  resetSeparationVariables() {
    this.productionManagementSeparationForm.reset();
    this.pmSeparation_SeparationId = 0;
    this.isProductionManagementSeparationFormSubmitted = false;
    this.pmSeparationsModel = {};
  }

  clearSeparation() {
    document.querySelector('#btnSeparation').innerHTML = 'Add';
    document.getElementById('btnCancelSeparation').style.display = 'none';
    this.resetSeparationVariables();
  }

  // step-3) Production Status
  getAllStatus() {
    try {
      this.appService.getAllStatus().subscribe(result => {
        if (result.StatusCode === 1) {
          this.pmStatuses = result.Data;
        } else {
          this.pmStatuses = [];
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      this.isLoading = false;
      this.toastr.error(this.errorMessage, '');
    }
  }

  saveStatus() {
    try {
      this.isProductionManagementStatusFormSubmitted = true;
      if (this.productionManagementStatusForm.valid) {
        if (this.pmStatus_ProductionStatusId === 0) {
          const productionStatusName = this.productionManagementStatusForm.controls['ProductionStatusName'].value;
          this.appService.saveStatus(productionStatusName).subscribe(result => {
            if (result.StatusCode === 1) {
              this.resetStatusVariables();
              this.getAllStatus();
              this.toastr.success('Save successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else {
          this.pmStatusesModel.StatusName = this.productionManagementStatusForm.controls['ProductionStatusName'].value;
          this.pmStatusesModel.ProductionStatusId = this.pmStatus_ProductionStatusId;
          this.appService.updateStatus(this.pmStatusesModel).subscribe(result => {
            if (result.StatusCode === 1) {
              this.clearStatus();
              this.getAllStatus();
              this.toastr.success('Save successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        }
      } else {
        return;
      }
    } catch (error) {
      console.log(error);
    }

  }

  editStatus(PMStatus: any) {
    try {
      document.querySelector('#btnStatus').innerHTML = 'Update';
      document.getElementById('btnCancelStatus').style.display = 'inline-block';
      this.pmStatus_ProductionStatusId = PMStatus.ProductionStatusId;
      this.productionManagementStatusForm.controls['ProductionStatusName'].setValue(PMStatus.Name);
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  deleteStatus(objPMStatus: any) {
    try {
      Swal({
        title: 'Are you sure you want to delete this production ?',
        text: '',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true,
        focusConfirm: false
      }).then((res) => {
        if (res.value) {
          this.appService.deleteStatus(objPMStatus.ProductionStatusId).subscribe(result => {
            if (result.StatusCode === 1) {
              document.querySelector('#btnStatus').innerHTML = 'Add';
              this.productionManagementStatusForm.reset();
              this.pmStatus_ProductionStatusId = 0;
              this.getAllStatus();
              this.toastr.success('Deleted successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else if (res.dismiss === Swal.DismissReason.cancel) {
        }
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  resetStatusVariables() {
    this.productionManagementStatusForm.reset();
    this.pmStatus_ProductionStatusId = 0;
    this.isProductionManagementStatusFormSubmitted = false;
    this.pmStatusesModel = {};
  }

  clearStatus() {
    document.querySelector('#btnStatus').innerHTML = 'Add';
    document.getElementById('btnCancelStatus').style.display = 'none';
    this.resetStatusVariables();
  }

  // step-4) Production Stages
  getAllStages() {
    try {
      this.appService.getAllStages().subscribe(result => {
        if (result.StatusCode === 1) {
          this.pmStages = result.Data;
        } else {
          this.pmStages = [];
        }
      }, error => {
        this.isLoading = false;
        this.toastr.error(this.errorMessage, '');
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  saveStage() {
    try {
      this.isProductionManagementStageFormSubmitted = true;
      if (this.productionManagementStageForm.valid) {
        if (this.pmStage_ProductionStageId === 0) {
          const productionStageName = this.productionManagementStageForm.controls['ProductionStageName'].value;
          this.appService.saveStages(productionStageName).subscribe(result => {
            if (result.StatusCode === 1) {
              this.resetStageVariables();
              this.getAllStages();
              this.toastr.success('Save successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else {
          this.pmStagesModel.StageName = this.productionManagementStageForm.controls['ProductionStageName'].value;
          this.pmStagesModel.ProductionStageId = this.pmStage_ProductionStageId;
          this.appService.updateStages(this.pmStagesModel).subscribe(result => {
            if (result.StatusCode === 1) {
              this.clearStage();
              this.getAllStages();
              this.toastr.success('Save successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        }
      } else {
        return;
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  editStage(PMStage: any) {
    try {
      document.querySelector('#btnStage').innerHTML = 'Update';
      document.getElementById('btnCancelStage').style.display = 'inline-block';
      this.pmStage_ProductionStageId = PMStage.ProductionStageId;
      this.productionManagementStageForm.controls['ProductionStageName'].setValue(PMStage.Name);
    } catch (error) {
      this.isLoading = false;
      this.toastr.error(this.errorMessage, '');
    }

  }

  deleteStage(objPMStage: any) {
    try {
      Swal({
        title: 'Are you sure you want to delete this production ?',
        text: '',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true,
        focusConfirm: false
      }).then((res) => {
        if (res.value) {
          this.appService.deleteStages(objPMStage.ProductionStageId).subscribe(result => {
            if (result.StatusCode === 1) {
              document.querySelector('#btnStage').innerHTML = 'Add';
              this.productionManagementStageForm.reset();
              this.pmStage_ProductionStageId = 0;
              this.getAllStages();
              this.toastr.success('Deleted successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else if (res.dismiss === Swal.DismissReason.cancel) {
        }
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  resetStageVariables() {
    this.productionManagementStageForm.reset();
    this.pmStage_ProductionStageId = 0;
    this.isProductionManagementStageFormSubmitted = false;
    this.pmStagesModel = {};
  }

  clearStage() {
    document.querySelector('#btnStage').innerHTML = 'Add';
    document.getElementById('btnCancelStage').style.display = 'none';
    this.resetStageVariables();
  }

}

